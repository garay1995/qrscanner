import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DecodeQrComponent } from './scanerQr/decode-qr/decode-qr.component';
import { EncodeQrComponent } from './scanerQr/encode-qr/encode-qr.component';

import { NgQRCodeReaderModule } from 'ng2-qrcode-reader';
import { NgQrScannerModule } from 'angular2-qrscanner';



@NgModule({
  declarations: [
    AppComponent,
    DecodeQrComponent,
    EncodeQrComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgQRCodeReaderModule,
    NgQrScannerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
