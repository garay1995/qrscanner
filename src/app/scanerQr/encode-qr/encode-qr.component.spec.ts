import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EncodeQrComponent } from './encode-qr.component';

describe('EncodeQrComponent', () => {
  let component: EncodeQrComponent;
  let fixture: ComponentFixture<EncodeQrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EncodeQrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EncodeQrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
