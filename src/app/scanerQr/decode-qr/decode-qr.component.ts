import { Component, ElementRef, OnInit, Renderer2, ViewChild, ViewEncapsulation } from '@angular/core';
import { QrScannerComponent } from 'angular2-qrscanner';
import { FuncionesService } from 'src/app/utils/funciones.service';


@Component({
  selector: 'app-decode-qr',
  templateUrl: './decode-qr.component.html',
  styleUrls: ['./decode-qr.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class DecodeQrComponent implements OnInit  {
 
 
   title = 'Decodificador imágenes QR';  
 
  view : boolean = false;
  notV : boolean = true;
  afterscan : boolean = true;
 
  resultElement: any;
  
  @ViewChild(QrScannerComponent, { static : false }) qrScannerComponent: QrScannerComponent ;

  constructor() {  
  }  


  ngOnInit() {
        
  }

  reload(){
    this.notV = true;
      this.ngAfterViewInit();
      this.view = false;
      
  }
  ngAfterViewInit(): void {
      
    this.qrScannerComponent.getMediaDevices().then(devices => {
      //console.log(devices);
      const videoDevices: MediaDeviceInfo[] = [];
      for (const device of devices) {
          if (device.kind.toString() === 'videoinput') {
              videoDevices.push(device);
          }
      }
      if (videoDevices.length > 0){
          let choosenDev;
          for (const dev of videoDevices){
              if (dev.label.includes('front')){
                  choosenDev = dev;
                  break;
              }
          }
          if (choosenDev) {
              this.qrScannerComponent.chooseCamera.next(choosenDev);
          } else {
              this.qrScannerComponent.chooseCamera.next(videoDevices[0]);
          }
      }
  });

  this.qrScannerComponent.capturedQr.subscribe(result => {
    this.view = true;
    this.notV = false;
    this.resultElement = result;
  });
  }

}
