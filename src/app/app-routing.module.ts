import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DecodeQrComponent } from './scanerQr/decode-qr/decode-qr.component';

const routes: Routes = [
  
  
  { path: 'decodeQr', component: DecodeQrComponent },
  { path: '**', redirectTo: 'decodeQr', pathMatch: 'full'}



];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
